﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour
{

    enum MoveDirection { up, down, left, right, none };

    const MoveDirection DIR_UP = MoveDirection.up;
    const MoveDirection DIR_DOWN = MoveDirection.down;
    const MoveDirection DIR_LEFT = MoveDirection.left;
    const MoveDirection DIR_RIGHT = MoveDirection.right;
    const MoveDirection DIR_NONE = MoveDirection.none;


    public int Columns = 4;
    public int Rows = 4;

    public float FastSwipeSeconds = 0.1f;
    public float NormalSwipeSeconds = 0.1f;

    public int MinSortMoves = 30;
    public int MaxSortMoves = 90;
    [Range(1, 10)]
    public float MoveKoeficient = 3.0f;
    [Range(1, 10)]
    public float TimeForOneMove = 2.0f;
    
    public int InitSeconds
    {
        get
        {
            return (int)Mathf.Round(movesNeedToWin * TimeForOneMove);
        }
    }

    public int PlayerMoves
    {
        get
        {
            return (int)Mathf.Round(movesNeedToWin * MoveKoeficient);
        }
    }

    public int MovesWasMade
    {
        get
        {
            return movesWasMade;
        }
    }

    public GameObject CellPrefab;

    private Cell[,] map;
    private List<Cell> tmpSortList;
    private int movesNeedToWin = 0, movesWasMade;
    private int emptyCellRow, emptyCellColumn;
    private MoveDirection lastMoveDirection;

    void Awake()
    {
        tmpSortList = new List<Cell>(4);        
    }

    public IEnumerator CreateBoard()
    {
        map = new Cell[Columns, Rows];
        int lastColumn = Columns - 1;
        int lastRow = Rows - 1;

        float colInUCoord = 1.0f / Columns;
        float rowInVCoord = 1.0f / Rows;

        for (int column = 0; column < Columns; column++)
        {
            for (int row = 0; row < Rows; row++)
            {
                if (column == lastColumn && row == lastRow)
                    break;

                GameObject cellObj = Instantiate(CellPrefab,
                    new Vector3(row * 1.1f, 0, -column * 1.1f),
                    Quaternion.identity) as GameObject;

                cellObj.transform.parent = gameObject.transform;                
                cellObj.name = "Cell_" + column + '_' + row;
                Cell cell = cellObj.GetComponent<Cell>();
                cell.CellId = column * Rows + row;
                cell.setCurrentPosition(column, row);

                TextureAndMeshGenerator tmg =cellObj.GetComponent<TextureAndMeshGenerator>();
                if (tmg != null)
                {

                    tmg.initUW(
                        colInUCoord * (Columns - column - 1),
                        rowInVCoord * row,
                        colInUCoord * (Columns - column ),
                        rowInVCoord * (row + 1)
                        );
                }

                map[column, row] = cell;
                yield return new WaitForSeconds(0.05f);

            }
        }
        emptyCellColumn = Columns - 1; emptyCellRow = Rows - 1;
        map[emptyCellColumn, emptyCellRow] = null;       
    }

    public void ResetMovesWasMade()
    {
        movesWasMade = 0;
    }

    public IEnumerator SortOneCell(float moveSpeed)
    {
        tmpSortList.Clear();

        // inser in tmpSortList all exist neighborhood cells (near empty cell)

        if (lastMoveDirection != DIR_LEFT && isCellInBorder(emptyCellColumn, emptyCellRow + 1))
        {
            tmpSortList.Add(map[emptyCellColumn, emptyCellRow + 1]);
        }

        if (lastMoveDirection != DIR_RIGHT && isCellInBorder(emptyCellColumn, emptyCellRow - 1))
        {
            tmpSortList.Add(map[emptyCellColumn, emptyCellRow - 1]);
        }

        if (lastMoveDirection != DIR_UP && isCellInBorder(emptyCellColumn + 1, emptyCellRow))
        {
            tmpSortList.Add(map[emptyCellColumn + 1, emptyCellRow]);
        }

        if (lastMoveDirection != DIR_DOWN && isCellInBorder(emptyCellColumn - 1, emptyCellRow))
        {
            tmpSortList.Add(map[emptyCellColumn - 1, emptyCellRow]);
        }

        // now get random cell from selected neighborhood
        Cell swapingCell = tmpSortList[Random.Range(0, tmpSortList.Count)];

        int newEmptyCellCol = swapingCell.CurrentColumn;
        int newEmptyCellRow = swapingCell.CurrentRow;


        // next time we skip this diretion
        lastMoveDirection = getOpositeDirection(getDirection(
            newEmptyCellCol,
            newEmptyCellRow,
            emptyCellColumn,
            emptyCellRow
            ));
        //Debug.Log(lastMoveDirection);


        yield return StartCoroutine(
            swapingCell.Swipe(new Vector3(emptyCellRow * 1.1f, 0, -emptyCellColumn * 1.1f),
            moveSpeed,
            emptyCellColumn,
            emptyCellRow)
            );
        swapCellToEmpty(swapingCell, newEmptyCellCol, newEmptyCellRow);
    }

    public IEnumerator SortCells()
    {
        movesNeedToWin = Random.Range(MinSortMoves, MaxSortMoves);
        // this direction is skip on next move(prevent moving in oposite side)
        lastMoveDirection = DIR_NONE;
        for (int count = 0; count < movesNeedToWin; count++)
        {
            yield return StartCoroutine(SortOneCell(FastSwipeSeconds));
        }
    }

    private static MoveDirection getOpositeDirection(MoveDirection direction)
    {
        switch (direction)
        {
            case MoveDirection.left:
                return DIR_RIGHT;
            case MoveDirection.right:
                return DIR_LEFT;
            case MoveDirection.up:
                return DIR_DOWN;
            case MoveDirection.down:
                return DIR_UP;

            case MoveDirection.none:
            default:
                return DIR_NONE;
        }
    }

    private static MoveDirection getDirection(int cellCol, int cellRow, int newCol, int newRow)
    {
        int colDiff = newCol - cellCol;
        int rowDiff = newRow - cellRow;
        if (colDiff != 0)
        {
            switch (colDiff)
            {
                case 1:
                    return DIR_DOWN;
                case -1:
                    return DIR_UP;
            }
        }

        if (rowDiff != 0)
        {
            switch (rowDiff)
            {
                case 1:
                    return DIR_RIGHT;
                case -1:
                    return DIR_LEFT;
            }
        }

        return DIR_NONE;
    }

    private void swapCellToEmpty(Cell swapingCell, int newEmptyCellCol, int newEmptyCellRow)
    {
        // move cell on new position
        map[emptyCellColumn, emptyCellRow] = swapingCell;
        map[newEmptyCellCol, newEmptyCellRow] = null;
        emptyCellColumn = newEmptyCellCol;
        emptyCellRow = newEmptyCellRow;
    }

    public IEnumerator trySwipeCell(Cell cell,Vector3 swPos)
    {        
        if (isCellNearEmptyAndCorrectSwipeDirection(cell, swPos))
        {
            int newEmptyCellCol = cell.CurrentColumn;
            int newEmptyCellRow = cell.CurrentRow;

            yield return StartCoroutine(
               cell.Swipe(new Vector3(emptyCellRow * 1.1f, 0, -emptyCellColumn * 1.1f),
               NormalSwipeSeconds,
               emptyCellColumn,
               emptyCellRow)
               );
            swapCellToEmpty(cell, newEmptyCellCol, newEmptyCellRow);
            // increment moves was made
            movesWasMade++;
        }        
    }

    public bool isCellNearEmptyAndCorrectSwipeDirection(Cell cell, Vector3 swPos)
    {
        int diffCol = cell.CurrentColumn - emptyCellColumn;
        int diffRow = cell.CurrentRow - emptyCellRow;

        int moveRow = 0; int moveCol = 0;
        if (Mathf.Abs(swPos.x) > Mathf.Abs(swPos.y))
        {
            if (swPos.x > 0) { moveRow = 1; } else { moveRow = -1;}
        } 
        else
        {
            if (swPos.y < 0) { moveCol = 1; } else { moveCol = -1; }
        }        

        // if swipe in other direction
        if ((moveRow != diffRow) || (moveCol != diffCol))
            return false;
        
        int diffColAbs = Mathf.Abs(cell.CurrentColumn - emptyCellColumn);
        int diffRowAbs = Mathf.Abs(cell.CurrentRow - emptyCellRow);
        return (diffColAbs + diffRowAbs == 1);
    }
    
    private bool isCellInBorder(int column, int row)
    {
        return (column >= 0 && column < Columns && row >= 0 && row < Rows);
    }
    
    public bool isWin()
    {
        //win when bottom right is empty
        if (map[Columns - 1, Rows - 1] != null)
        {
            return false;
        }

        // and all cell in it own position
        for (int column = 0; column < Columns; column++)
        {
            for (int row = 0; row < Rows; row++)
            {
                // skip last empty cell
                if (map[column, row] == null)
                {
                    continue;
                }

                if (!this.isCorrectPosition(map[column, row], column, row))
                {
                    return false;
                }

            }
        }
        return true;
    }



    public bool isCorrectPosition(Cell cell, int column, int row)
    {
        if (cell.CellId == column * Rows + row + 1)
        {
            return true;
        }

        return false;
    }




    // Update is called once per frame
    void Update()
    {

    }
}
