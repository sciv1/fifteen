﻿using UnityEngine;
using System.Collections;

public class TextureAndMeshGenerator : MonoBehaviour {

    MeshFilter meshFilter;
    Mesh mesh;
	// Use this for initialization
	void Awake() {
        meshFilter = GetComponent<MeshFilter>();
        mesh = new Mesh();
        meshFilter.mesh = mesh;
        // 4 vertex
        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(-0.5f, 0, -0.5f);
        vertices[1] = new Vector3(0.5f, 0, -0.5f);
        vertices[2] = new Vector3(-0.5f, 0, 0.5f);
        vertices[3] = new Vector3(0.5f, 0, 0.5f);
        mesh.vertices = vertices;

        // two triangle
        int[] tri = new int[6];
        tri[0] = 0;
        tri[1] = 2;
        tri[2] = 1;

        tri[3] = 2;
        tri[4] = 3;
        tri[5] = 1;
        mesh.triangles = tri;

        Vector3[] normals = new Vector3[4];
        normals[0] = Vector3.up;
        normals[1] = Vector3.up;
        normals[2] = Vector3.up;
        normals[3] = Vector3.up;

        mesh.normals = normals;

    }

    public void initUW(float u1, float v1, float u2, float v2)
    {
        Vector2[] uv = new Vector2[4];
        
        uv[0] = new Vector2(v1, u1);
        uv[1] = new Vector2(v2, u1);
        uv[2] = new Vector2(v1, u2);
        uv[3] = new Vector2(v2, u2);

        mesh.uv = uv;
    }

	
	// Update is called once per frame
	void Update () {
	
	}
}
