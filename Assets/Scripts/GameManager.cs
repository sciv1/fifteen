﻿using UnityEngine;
using System.Collections;

public class GameManager : GlobalSingletonBehaviour<GameManager>
{
    public enum GameMode { Move, Time };
    private GameMode currentGameMode;

    public override void DoAwake()
    {
        currentGameMode = GameMode.Time;
    }

    public GameMode CurrentGameMode
    {
        get
        {            
            return currentGameMode;
        }

        set
        {
            currentGameMode = value;
        }

    }
}
