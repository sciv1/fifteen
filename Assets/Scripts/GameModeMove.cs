﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;

public class GameModeMove : MonoBehaviour, IGameModeType
{    
    public GameObject LoseDialog;
    private StringBuilder sb;
    public Text stepCounterText;

    const String MoveText = "<color=#ff0000ff>Moves:</color>\n<color=#00ff00ff>";
    const String MoveTextEnd = "</color>";

    private Board board;
    private int moveMax, stepCounter, counterRendered;   

    // Use this for initialization
    void Awake()
    {
        sb = new StringBuilder(64);
    }   

    // Update is called once per frame
    void Update()
    {
        updateMoveLeftCouterText();
    }


    public void InitUI(Board board)
    {
        this.board = board;

        moveMax = this.board.PlayerMoves;
        stepCounter = moveMax - this.board.MovesWasMade;
        counterRendered = -1;
        updateMoveLeftCouterText();

        stepCounterText.gameObject.SetActive(true);
        enabled = true;
    }

    public void StopMode()
    {
        enabled = false;
        stepCounterText.gameObject.SetActive(false);
    }

    public bool isLose()
    {
        return this.board.MovesWasMade >= moveMax;
    }

    public void showLoseDialog()
    {
        stepCounterText.gameObject.SetActive(false);
        LoseDialog.SetActive(true);
    }

    void updateMoveLeftCouterText()
    {
        stepCounter = moveMax - this.board.MovesWasMade;
        if (stepCounter != counterRendered)
        {
            sb.Length = 0;
            sb.Append(MoveText);
            sb.AppendFormat("{0:000}", stepCounter);
            sb.Append(MoveTextEnd);
            stepCounterText.text = sb.ToString();

            counterRendered = stepCounter;
        }
    }


    
    public void OnPause()
    {
        stepCounterText.gameObject.SetActive(false);
    }

    public void OnResume()
    {
        stepCounterText.gameObject.SetActive(true);
    }
}
