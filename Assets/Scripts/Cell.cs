﻿using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour {
    private int cellId;
    private TextMesh text;
    private Vector3 swipeStartPos;
    private Transform transformC;
    private int currentColumn, currentRow;

    public int CurrentColumn
    {
        get
        {
            return currentColumn;
        }
    }

    public int CurrentRow
    {
        get
        {
            return currentRow;
        }
    }

    public int CellId
    {
        get
        {
            return cellId;
        }
        set
        {
            cellId = value + 1;
            if (text != null)
            {
                text.text = cellId.ToString();
            }            
        }
    }

	// Use this for initialization
	void Awake() {
        swipeStartPos = new Vector3();
        transformC = GetComponent<Transform>();
        text = GetComponentInChildren<TextMesh>();
    }

    public IEnumerator Swipe(Vector3 target, float swipeTime, int col, int row)
    {
        float i = 0.0f;
        float rate = 1.0f / swipeTime;
        swipeStartPos.Set(transformC.position.x, transformC.position.y, transformC.position.z);
        while (i < 1.0)
        {
            i += Time.deltaTime * rate;
            transformC.position = Vector3.Lerp(swipeStartPos, target, i);
            yield return null;
        }
        setCurrentPosition(col, row);
    }

    public void setCurrentPosition(int col, int row)
    {
        currentColumn = col;
        currentRow = row;
    }
    
   

}
