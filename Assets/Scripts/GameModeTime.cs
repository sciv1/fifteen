﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;

public class GameModeTime : MonoBehaviour, IGameModeType
{
    public Text TimerText;
    public GameObject LoseDialog;

    const String timerText = "<color=#ff0000ff>Time:</color>\n<color=#00ff00ff>";
    const String timerTextEnd = "</color>";
  
    private StringBuilder sb;    

    float timer;
    public void InitUI(Board board)
    {             
        enabled = true;
        TimerText.gameObject.SetActive(true);
        timer = board.InitSeconds;
    }

    public void StopMode()
    {
        enabled = false;
        TimerText.gameObject.SetActive(false);
    }

    public bool isLose()
    {
        return timer <= 1;
    }

    public void showLoseDialog()
    {
        TimerText.gameObject.SetActive(false);
        LoseDialog.SetActive(true);
    }

    // Use this for initialization
    void Awake () {
        sb = new StringBuilder(64);
    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            timer = 0;
        }

        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        sb.Length = 0;
        sb.Append(timerText);
        sb.AppendFormat("{0:00}:{1:00}", minutes, seconds);
        sb.Append(timerTextEnd);
        TimerText.text = sb.ToString();
    }

    public void OnPause()
    {
        TimerText.gameObject.SetActive(false);
        enabled = false;
    }

    public void OnResume()
    {
        TimerText.gameObject.SetActive(true);
        enabled = true;
    }
}
