﻿using UnityEngine;

public class AutoSingletonBehaviour<T> : MonoBehaviour where T : AutoSingletonBehaviour<T>
{
	private static T instance;
	protected bool awaken;
	
	public static T Instance
	{
		get { return AutoSingletonBehaviour<T>.instance != null ? AutoSingletonBehaviour<T>.instance : AutoSingletonBehaviour<T>.Load(); }
		set { AutoSingletonBehaviour<T>.instance = value; }
	}
	
	private static T Load()
	{
		var instance = (T)FindObjectOfType(typeof(T));
		if (instance == null)
		{
			var obj = new GameObject(typeof(T).Name);
			instance = obj.AddComponent<T>();
		}
		instance.Awake();
		return instance;
	}
	
	public void Awake()
	{
		if (this.awaken)
		{
			return;
		}
		this.awaken = true;
		
		if (AutoSingletonBehaviour<T>.instance != null && AutoSingletonBehaviour<T>.instance != this)
		{
			Object.Destroy(this.gameObject);
			return;
		}
		
		AutoSingletonBehaviour<T>.instance = (T)this;
		this.DoAwake();
	}
	
	public void OnDestroy()
	{
		if (AutoSingletonBehaviour<T>.instance == this)
		{
			AutoSingletonBehaviour<T>.instance = null;
			this.DoDestroy();
		}
	}
	
	public virtual void DoAwake()
	{
	}
	
	public virtual void DoDestroy()
	{
	}
}