﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PuzzleGameController : AutoSingletonBehaviour<PuzzleGameController>
{
    public Board board;
    public PlayerController playerController;

    public GameModeMove GameModeMove;
    public GameModeTime GameModeTime;
    public GameObject WinDialog;
    public GameObject MenuDialog;
    public GameObject MenuButton;

    public enum GameState {Start, FirstTouch, Play, Menu, Win, Lose};

    const GameState GameStateStart = GameState.Start;
    const GameState GameStatePlay = GameState.Play;
    const GameState GameStateMenu = GameState.Menu;
    const GameState GameStateWin = GameState.Win;
    const GameState GameStateLose = GameState.Lose;

    private GameState currentGameState;
    private IGameModeType gameMode;

    public GameState CurrentGameState
    {
        
        get
        {
            return currentGameState;
        }

        set
        {
            currentGameState = value;
            switch (currentGameState)
            {
                case GameState.Start:
                    StartCoroutine(InitGame());                    
                    break;

                case GameState.Play:
                    MenuDialog.SetActive(false);
                    MenuButton.SetActive(true);
                    playerController.enabled = true;
                    gameMode.OnResume();
                    break;

                case GameState.Menu:
                    MenuButton.SetActive(false);
                    MenuDialog.SetActive(true);
                    playerController.enabled = false;
                    gameMode.OnPause();
                    break;

                case GameState.Win:
                    MenuButton.SetActive(false);
                    StartCoroutine(WinGame());
                    break;
                case GameState.Lose:
                    MenuButton.SetActive(false);
                    playerController.enabled = false;
                    gameMode.StopMode();
                    gameMode.showLoseDialog();
                    break;
            }
        }
    }


    // Use this for initialization
    void Start () {
        CurrentGameState = GameStateStart;
    }


    private IEnumerator InitGame()
    {
        playerController.enabled = false;
        yield return StartCoroutine(board.CreateBoard());
        yield return StartCoroutine(board.SortCells());

        GameManager.GameMode gMode = GameManager.Instance.CurrentGameMode;        
        switch(gMode)
        {
            case GameManager.GameMode.Move:
                gameMode = GameModeMove;
                break;                
            case GameManager.GameMode.Time:
                gameMode = GameModeTime;
                break;
        }
        gameMode.InitUI(board);
        playerController.enabled = true;
        CurrentGameState = GameStatePlay;
    }

    private IEnumerator WinGame()
    {
        //disable lose mechanism and it ui
        gameMode.StopMode();
        WinDialog.SetActive(true);
        yield return null;
    }

    public void PauseGame()
    {
        if (CurrentGameState == GameStatePlay)
        {
            CurrentGameState = GameStateMenu;
        }
    }

    public void ResumeGame()
    {
        if (CurrentGameState == GameStateMenu )
        {
            CurrentGameState = GameStatePlay;
        }
    }


    public IEnumerator TouchCell(Cell cell, Vector3 swPos)
    {
        playerController.enabled = false;
        yield return StartCoroutine(board.trySwipeCell(cell, swPos));

        if (board.isWin())
        {
            CurrentGameState = GameStateWin;
        }
        else
        {
            playerController.enabled = true;
        }       
    }

	
	// Update is called once per frame
	void Update () {
	    switch (CurrentGameState)
        {
            case GameStatePlay:
                if (gameMode.isLose())
                {
                    CurrentGameState = GameStateLose;
                }
                break;
        }
	}

    public void RestartGame()
    {
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
    }

    public void GoToMain()
    {
        SceneManager.LoadScene("Main");
    }
}
