﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour {
    public Board board;
    private WaitForSeconds wait;


    void Start()
    {
        wait = new WaitForSeconds(1.0f);
        StartCoroutine(InitBackground());
    }

    private IEnumerator InitBackground()
    {        
        yield return StartCoroutine(board.CreateBoard());
        while(true)
        {
            yield return StartCoroutine(board.SortOneCell(board.NormalSwipeSeconds));            
            yield return wait;
        }
        
    }

    public void PlayGameOnMoves()
    {
        GameManager.Instance.CurrentGameMode = GameManager.GameMode.Move;
        SceneManager.LoadScene("Puzzle");
    }

    public void PlayGameOnTime()
    {
        GameManager.Instance.CurrentGameMode = GameManager.GameMode.Time;
        SceneManager.LoadScene("Puzzle");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
    }
}
