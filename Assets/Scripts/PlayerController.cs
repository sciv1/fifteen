﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    [Range(0.0f, 0.3f)]
    public float SwipeDistanceInScreenCoord = 0.1f;
    private Vector3 touchOrigin;	// Position of cursor when mouse dragging starts
    private Cell selectedCell;
    private void checkTouchDown(Vector3 pos)
    {
        touchOrigin = pos;
        Ray ray = Camera.main.ScreenPointToRay(pos);
        RaycastHit hit;        
        if (Physics.Raycast(ray, out hit) && hit.transform.gameObject.tag == "Cell")
        {
            selectedCell = hit.transform.gameObject.GetComponent<Cell>();
           /* StartCoroutine(PuzzleGameController.Instance.TouchCell(
                )
            );*/
        }
    }

    private void checkTouchUp(Vector3 pos)
    {
        if (selectedCell != null)
        {
            Vector3 swPos = Camera.main.ScreenToViewportPoint(touchOrigin - pos);           
            if (swPos.magnitude >= SwipeDistanceInScreenCoord)
            {
                StartCoroutine(PuzzleGameController.Instance.TouchCell(selectedCell, swPos
                         )
                     );
            }
            selectedCell = null;
        }
    }

    // Update is called once per frame
    void Update () {

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (Input.touchCount > 0)
            {

                switch (Input.GetTouch(0).phase)
                {
                    case TouchPhase.Began:                
                        checkTouchDown(Input.GetTouch(0).position);
                        break;
                    case TouchPhase.Ended:
                        checkTouchUp(Input.GetTouch(0).position);
                        break;
                } 
            }  
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                checkTouchDown(Input.mousePosition);
            }
            else if (Input.GetMouseButtonUp(0))
                   {
                        checkTouchUp(Input.mousePosition);
                   } 
        }
    }
}
