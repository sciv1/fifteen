﻿using UnityEngine;

public class GlobalSingletonBehaviour<T> : MonoBehaviour where T : GlobalSingletonBehaviour<T>
{
	static bool destroyed;
	static T instance;
	protected bool awaken;
	
	public static T Instance
	{
		get { return GlobalSingletonBehaviour<T>.instance != null || destroyed ? GlobalSingletonBehaviour<T>.instance : GlobalSingletonBehaviour<T>.Load(); }
		set { GlobalSingletonBehaviour<T>.instance = value; }
	}
	
	private static T Load()
	{
		var instance = (T)FindObjectOfType(typeof(T));
		if (instance == null)
		{
			var obj = new GameObject(typeof(T).Name);
			instance = obj.AddComponent<T>();
		}
		instance.Awake();
		return instance;
	}
	
	public void Awake()
	{
		if (this.awaken)
		{
			return;
		}
		this.awaken = true;
		
		if (GlobalSingletonBehaviour<T>.instance != null && GlobalSingletonBehaviour<T>.instance != this)
		{
			Object.Destroy(this.gameObject);
			return;
		}
		
		GlobalSingletonBehaviour<T>.instance = (T)this;
		Object.DontDestroyOnLoad(this.gameObject);
		this.DoAwake();
	}
	
	public void OnDestroy()
	{
		if (GlobalSingletonBehaviour<T>.instance == this)
		{
			destroyed = true;
			this.DoDestroy();
		}
	}
	
	public virtual void DoAwake()
	{
	}
	
	public virtual void DoDestroy()
	{
	}
}
