﻿using UnityEngine;
using System.Collections;

public class AboutPanel : MonoBehaviour {

    RectTransform rectTransform;
    Vector2 tmp;
    // Use this for initialization
    bool canShowAutor;
    bool canHideAutor;

    private IEnumerator showAndHideCoroutine;

    void Awake () {
        canShowAutor = true;
        tmp = new Vector3();
        rectTransform = GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector3(210, 0, 0);
        gameObject.SetActive(false);
    }

    public void ShowAutor()
    {
        if (canShowAutor)
        {
            canShowAutor = false;

            rectTransform.anchoredPosition = new Vector3(210, 0, 0);
            gameObject.SetActive(true);
            showAndHideCoroutine = ShowAndHideAutor();
            StartCoroutine(showAndHideCoroutine);
        }
    }

    public void OnClickHide()
    {
        if (canHideAutor)
        {
            canHideAutor = false;
            StopCoroutine(showAndHideCoroutine);
            StartCoroutine(HideAutor());
        }
    }


    IEnumerator ShowAndHideAutor()
    {
        // show autor bloclk
        yield return StartCoroutine(moveTo(new Vector2(-20, 0)));
        canHideAutor = true;
        yield return new WaitForSeconds(5.0f);
        canHideAutor = false;
        yield return StartCoroutine(moveTo(new Vector2(210, 0)));
        gameObject.SetActive(false);
        canShowAutor = true;
    }

    IEnumerator HideAutor()
    {
        // hide autor bloclk       
        yield return StartCoroutine(moveTo(new Vector2(210, 0)));
        gameObject.SetActive(false);
        canShowAutor = true;
    }


    IEnumerator moveTo(Vector2 target)
    {
        tmp.Set(
            rectTransform.anchoredPosition.x,
            rectTransform.anchoredPosition.y
            );
        float i = 0;
        while (i < 1)
        {
            i += Time.deltaTime;
            rectTransform.anchoredPosition = Vector2.Lerp(tmp, target, i);
            yield return null;
        }

        rectTransform.anchoredPosition.Set(
            target.x,
            target.y            
            );
    }

	
}
