﻿public interface IGameModeType
{
    void InitUI(Board board);
    void StopMode();
    bool isLose();
    void showLoseDialog();
    void OnPause();
    void OnResume();
}

